<a href="{{route($route)}}" class="btn btn-primary pull-right">
	@lang('messages.BUTTON_NEW') <i class="glyphicon glyphicon-plus"></i>
</a>