<button class="btn btn-primary pull-right" type="submit">
	@lang('messages.BUTTON_SAVE') <i class="glyphicon glyphicon-floppy-save"></i>
</button>