@extends('layouts.app', [
    'headerImage' => 'home-bg.jpg'
])

@section('content')
  <h2>Posts</h2>
  <p>All Posts in System</p>
  @include('components.buttons.new', ['route' => 'post.new'])

  <table class="table table-striped">
    <thead>
      <tr>
        <th>title</th>
        <th>subtitle</th>
        <th>url</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
    @forelse ($posts as $post)
      <tr>
        <td>{{$post->title}}</td>
        <td>{{$post->subtitle}}</td>
        <td>{{$post->url}}</td>
        <td>
          @include('components.anchor.edit', ['link' => route('post.edit', $post->id)])
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="4"><center>No Posts</center></td>
      </tr>
    @endforelse
    </tbody>
  </table>
  <?php echo $posts->render(); ?>
@endsection