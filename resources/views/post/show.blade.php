@extends('layouts.app', [
    'headerImage' => 'post-bg.jpg',
    'title' => $post->title,
    'subtitle' => $post->subtitle,
    'page' => 'post',
    'author' => $post->user->name,
    'post' => $post
])

@section('content')
<!-- Post Content -->
    <article>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                {!! $post->text !!}
            </div>
        </div>
    </article>
@endsection