@extends('layouts.app', [
    'headerImage' => 'home-bg.jpg'
])

@section('content')
<!-- Post Content -->
    <div class="row omb_row-sm-offset-3">
        <!-- <div class="col-xs-12 col-sm-6">     -->
            <form method="POST" action="{{route('post.update', $post->id)}}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                	<label for="email">Title:</label>
                    <input id="title" type="text" class="form-control" name="title" value="{{$post->title}}" required autofocus>
                </div>
                <span class="help-block">
                    @if ($errors->has('email'))
                        <strong>{{ $errors->first('email') }}</strong>
                    @endif
                </span>
                                    
                <div class="form-group {{ $errors->has('subtitle') ? 'has-error' : '' }}">
                <label for="email">Subtitle:</label>
                    <input id="subtitle" type="text" class="form-control" name="subtitle" value="{{$post->subtitle}}" required>
                </div>
                <div class="form-group {{ $errors->has('text') ? 'has-error' : '' }}">
                    <!-- <span class="input-group-addon"><i class="fa fa-lock"></i></span> -->
                    <textarea id="text" name="text" class="ckeditorE">
                    	{!! $post->text !!}
                    </textarea>
                </div>
                <span class="help-block">
                    @if ($errors->has('password'))
                        <strong>{{ $errors->first('password') }}</strong>
                    @endif
                </span>
                @include('components.buttons.save')
            </form>
        <!-- </div> -->
    </div>
@endsection
@section('js')
    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
      <script>
        $('.ckeditorE').ckeditor();
    </script>
@endsection