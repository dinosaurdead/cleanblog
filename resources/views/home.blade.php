@extends('layouts.app', ['headerImage' => $headerImage])

@section('content')
<div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                @foreach($posts as $post)
                    <div class="post-preview">
                        <a href="{{route('post.show', ['friendlyUrl' => $post->url])}}">
                            <h2 class="post-title">
                                {{$post->title}}
                            </h2>
                            <h3 class="post-subtitle">
                                {{$post->subtitle}}
                            </h3>
                        </a>
                        <p class="post-meta">
                            Posted by <a href="#">{{$post->user->name}}</a> on {{$post->created_at->format('M d, Y')}}
                            @can('update', $post)
                                <a href="{{route('post.edit', $post->id)}}"> <i class="fa fa-edit"></i>Edit</a>
                            @endcan
                        </p>
                    </div>
                    <hr>
                @endforeach
                <!-- Pager -->
                <ul class="pager">
                    <li class="next">
                        <a href="#">Older Posts &rarr;</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
