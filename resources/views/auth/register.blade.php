@extends('layouts.auth')

@section('content')

<div class="row omb_row-sm-offset-3 omb_loginOr">
        <div class="col-xs-12 col-sm-6">
            <hr class="omb_hrOr">
            <span class="omb_spanOr">@lang('messages.TITLE_REGISTER')</span>
        </div>
    </div>

    <div class="row omb_row-sm-offset-3">
        <div class="col-xs-12 col-sm-6">    
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <div class="input-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input id="name" type="name" class="form-control" name="name" value="{{ old('name') }}" placeholder="@lang('messages.LABEL_NAME')" required autofocus>
                </div>
                <span class="help-block">
                    @if ($errors->has('name'))
                        <strong>{{ $errors->first('name') }}</strong>
                    @endif
                </span>
                <div class="input-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('messages.LABEL_EMAIL')" required autofocus>
                </div>
                <span class="help-block">
                    @if ($errors->has('email'))
                        <strong>{{ $errors->first('email') }}</strong>
                    @endif
                </span>
                <div class="input-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input id="password" type="password" class="form-control" name="password" placeholder="@lang('messages.LABEL_PASSWORD')" required>
                </div>
                <span class="help-block">
                    @if ($errors->has('password'))
                        <strong>{{ $errors->first('password') }}</strong>
                    @endif
                </span>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="@lang('messages.LABEL_PASSWORDCONFIRM')" required>
                </div>
                <span class="help-block"></span>

                @include('components.buttons.btnLgPrimaryBlock', ['text' => 'messages.BUTTON_REGISTER'])
            </form>
        </div>
    </div>
@endsection
