@extends('layouts.auth')

@section('content')
    <div class="row omb_row-sm-offset-3 omb_loginOr">
        <div class="col-xs-12 col-sm-6">
            <hr class="omb_hrOr">
            <span class="omb_spanOr">@lang('messages.TITLE_LOGIN')</span>
        </div>
    </div>

    <div class="row omb_row-sm-offset-3">
        <div class="col-xs-12 col-sm-6">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="input-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input id="email" type="textarea" class="form-control ckeditor" name="email" value="{{ old('email') }}" required autofocus>
                </div>
                <span class="help-block">
                    @if ($errors->has('email'))
                        <strong>{{ $errors->first('email') }}</strong>
                    @endif
                </span>
                <div class="input-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input id="password" type="password" class="form-control" name="password" required>
                </div>
                <span class="help-block">
                    @if ($errors->has('password'))
                        <strong>{{ $errors->first('password') }}</strong>
                    @endif
                </span>
                @include('components.buttons.btnLgPrimaryBlock', ['text' => 'messages.BUTTON_LOGIN'])
            </form>
        </div>
    </div>
    <div class="row omb_row-sm-offset-3">
        <div class="col-xs-12 col-sm-3">
            <label class="checkbox">
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>Remember Me
            </label>
        </div>
        <div class="col-xs-12 col-sm-3">
            <p class="omb_forgotPwd">
                <a href="{{ route('password.request') }}">Forgot Your Password?</a>
            </p>
        </div>
    </div>
@endsection