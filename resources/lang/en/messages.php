<?php
return [
	'BUTTON_LOGIN' => 'Login',
	'BUTTON_NEW' => 'New',
	'BUTTON_REGISTER' => 'Register',
	'BUTTON_SAVE' => 'Save',
	'LABEL_EMAIL' => 'Email',
	'LABEL_NAME' => 'Name',
	'LABEL_PASSWORD' => 'Password',
	'LABEL_PASSWORDCONFIRM' => 'Password Confirm',
	'TITLE_LOGIN' => 'Login',
	'TITLE_SIGNUP' => 'Sign up',
	'TITLE_REGISTER' => 'Register'
];