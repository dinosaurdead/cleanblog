<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/post', 'PostController@create')->name('post.create');
Route::get('/post', 'PostController@index')->name('post.index');
Route::get('post/{id}/edit', 'PostController@edit')->name('post.edit');
Route::get('post/new', 'PostController@new')->name('post.new');
Route::get('post/{friendlyUrl}', 'PostController@show')->name('post.show');
Route::post('post/{id}/update', 'PostController@update')->name('post.update');
