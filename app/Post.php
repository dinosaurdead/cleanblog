<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text', 'title', 'subtitle',
    ];

    /**
    * Get the user record associated with the Post.
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
    * Get the image record associated with the Post.
    */
    public function image()
    {
        return $this->hasOne('App\Image', 'id', 'image_id');
    }
}
