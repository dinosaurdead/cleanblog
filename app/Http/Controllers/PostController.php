<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create', 'index', 'edit', 'new', 'update']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        DB::table('posts')->insert([
            'title' => $request->input('title'),
            'subtitle' => $request->input('subtitle'),
            'text' => $request->input('text'),
            'user_id' => Auth::id(),
            'user_last_updated' => Auth::id(),
            'url' => str_slug($request->input('title')),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        return redirect()->route('post.index', ['posts' => Post::all()]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('post.index', [
            'posts' => Post::paginate(10),
            'headerImage' => 'home-bg.jpg'
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find(1)->whereRaw('id = ?', [$id])->first();

        $this->authorize('update', $post);
        return view('post.edit', [
            'post' => $post
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function new()
    {
        return view('post.new');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($friendlyUrl)
    {
        return view('post.show', [
            'post' => Post::find(1)->whereRaw('url = ?', [$friendlyUrl])->first()
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $this->authorize('update', $post);

    	$post->update([
            'title' => $request->input('title'),
            'subtitle' => $request->input('subtitle'),
            'text' => $request->input('text'),
            'user_last_updated' => Auth::id(),
            'url' => str_slug($request->input('title')),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        return redirect()->route('post.index', ['posts' => Post::all()]);
    }
}
