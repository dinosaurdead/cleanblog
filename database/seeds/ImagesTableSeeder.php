<?php

use Illuminate\Database\Seeder;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->insert([
            'thumbnail' => 'post-bg.jpg',
            'small' => 'post-bg.jpg',
            'medium' => 'post-bg.jpg',
            'large' => 'post-bg.jpg',
            'xlarge' => 'post-bg.jpg',
            'checksum' => md5_file(public_path('img/post-bg.jpg')),
        ]);
    }
}
